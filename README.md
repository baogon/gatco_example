# gatco_example aaa

## create database
```
$ sudo su postgres
# psql
postgres=# \l
postgres=# create database python_gatco_examp encoding='UTF-8';
CREATE DATABASE
postgres=# create user python_gatco_user with password '123456';
CREATE ROLE
postgres=# grant ALL privileges on database python_gatco_examp to python_gatco_user ;
GRANT

```
## create and clone project 
```
$ python3 -m venv gatco_example
$ cd gatco_example
$ git clone https://gitlab.com/baogon/gatco_example.git repo
$ cd repo
$ source ../bin/activate
$ pip install -r requirements.txt

```
## config database 
### application/config.py
```
class Config(object):
    DEBUG = True
    STATIC_URL = "/static"
    # SQLALCHEMY_DATABASE_URI = 'postgresql://quanlythanuser:123456@localhost:5432/quanlythandb'
    SQLALCHEMY_DATABASE_URI = 'postgresql://python_gatco_user:123456@localhost:5432/python_gatco_examp'
    AUTH_LOGIN_ENDPOINT = 'login'
    AUTH_PASSWORD_HASH = 'sha512_crypt'
    AUTH_PASSWORD_SALT = 'ruewhndjsa17heaw'
    SECRET_KEY = 'e2q8dhaushdauwd7qye'
    SESSION_COOKIE_SALT = 'dhuasud819wubadhysagd'

    AUTH_EXPIRE_TIME = 86400
    TOKEN_EXPIRED = 86400

    FS_ROOT = "static/upload/"
```
### alembic.ini 
```
sqlalchemy.url = postgresql://python_gatco_user:123456@localhost:5432/python_gatco_examp
```
## alembic database 
```
$ rm -rf alembic/versions/
$ mkdir alembic/version
$ alembic revision --autogenerate -m "init"
INFO  [alembic.runtime.migration] Context impl PostgresqlImpl.
INFO  [alembic.runtime.migration] Will assume transactional DDL.
INFO  [alembic.autogenerate.compare] Detected added table 'role'
INFO  [alembic.autogenerate.compare] Detected added index 'ix_role_name' on '['name']'
INFO  [alembic.autogenerate.compare] Detected added table 'users'
INFO  [alembic.autogenerate.compare] Detected added index 'ix_users_email' on '['email']'
INFO  [alembic.autogenerate.compare] Detected added index 'ix_users_user_name' on '['user_name']'
INFO  [alembic.autogenerate.compare] Detected added table 'roles_users'
  Generating /home/bao/Documents/Python_basic/gatco_example/repo/alembic/versions/3c555309a3d5_init.py ... done
$ alembic upgrade head 
INFO  [alembic.runtime.migration] Context impl PostgresqlImpl.
INFO  [alembic.runtime.migration] Will assume transactional DDL.
INFO  [alembic.runtime.migration] Running upgrade  -> 3c555309a3d5, init
```
