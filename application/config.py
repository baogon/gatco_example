class Config(object):
    DEBUG = True
    STATIC_URL = "/static"
    # SQLALCHEMY_DATABASE_URI = 'postgresql://quanlythanuser:123456@localhost:5432/quanlythandb'
    SQLALCHEMY_DATABASE_URI = 'postgresql://python_gatco_user:123456@localhost:5432/python_gatco_examp'
    AUTH_LOGIN_ENDPOINT = 'login'
    AUTH_PASSWORD_HASH = 'sha512_crypt'
    AUTH_PASSWORD_SALT = 'ruewhndjsa17heaw'
    SECRET_KEY = 'e2q8dhaushdauwd7qye'
    SESSION_COOKIE_SALT = 'dhuasud819wubadhysagd'

    AUTH_EXPIRE_TIME = 86400
    TOKEN_EXPIRED = 86400

    FS_ROOT = "static/upload/"
    

