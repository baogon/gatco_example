define(function (require) {
    "use strict";
    var $                   = require('jquery'),
        _                   = require('underscore'),
        Gonrin				= require('gonrin');
    
    var template 			= require('text!app/view/HoaDon/tpl/model.html'),
    	schema 				= require('json!schema/HoaDonSchema.json');
	var KhachHangSelectView   = require('app/view/KhachHang/SelectView');
	var ChiTietHoaDonModelView = require("app/view/HoaDon/ChiTietHoaDonModelView");
    return Gonrin.ModelView.extend({
    	template : template,
    	modelSchema	: schema,
    	urlPrefix: "/api/v1/",
    	collectionName: "hoadon",
		uiControl:{
    		fields:[
				{
					field: "chitiethoadon",
					uicontrol: false,
					itemView: ChiTietHoaDonModelView,
					tools: [{
						name: "create",
						type: "button",
						buttonClass: "btn btn-outline-success btn-sm",
						label: "<span class='btn btn-primary'>Add</span>",
						command: "create"
					}],
					toolEl: "#add_chitiet_hoadon"
				},
        		{
    				field:"khachhang",
    				uicontrol:"ref",
    				textField: "ten",
    				dataSource: KhachHangSelectView
    			},
        	]
    	},
    	tools : [
    	    {
    	    	name: "defaultgr",
    	    	type: "group",
    	    	groupClass: "toolbar-group",
    	    	buttons: [
					{
						name: "back",
						type: "button",
						buttonClass: "btn-default btn-sm",
						label: "TRANSLATE:BACK",
						command: function(){
							var self = this;
							
							Backbone.history.history.back();
						}
					},
					{
		    	    	name: "save",
		    	    	type: "button",
		    	    	buttonClass: "btn-success btn-sm",
		    	    	label: "Lưu hoa don",
		    	    	command: function(){
		    	    		var self = this;
		    	    		
		                    self.model.save(null,{
		                        success: function (model, respose, options) {
		                            self.getApp().notify("Lưu thông tin thành công");
		                            self.getApp().getRouter().navigate(self.collectionName + "/collection");
		                            
		                        },
		                        error: function (model, xhr, options) {
		                            self.getApp().notify('Lưu thông tin không thành công!');
		                           
		                        }
		                    });
		    	    	}
		    	    },
					{
		    	    	name: "delete",
		    	    	type: "button",
		    	    	buttonClass: "btn-danger btn-sm",
		    	    	label: "TRANSLATE:DELETE",
		    	    	visible: function(){
		    	    		return this.getApp().getRouter().getParam("id") !== null;
		    	    	},
		    	    	command: function(){
		    	    		var self = this;
		                    self.model.destroy({
		                        success: function(model, response) {
		                        	self.getApp().notify('Xoá dữ liệu thành công');
		                            self.getApp().getRouter().navigate(self.collectionName + "/collection");
		                        },
		                        error: function (model, xhr, options) {
		                            self.getApp().notify('Xoá dữ liệu không thành công!');
		                            
		                        }
		                    });
		    	    	}
		    	    },
    	    	],
    	    }],
    	render:function(){
    		var self = this;
    		var id = this.getApp().getRouter().getParam("id");
    		if(id){
    			//progresbar quay quay
    			this.model.set('id',id);
        		this.model.fetch({
        			success: function(data){
        				self.applyBindings();
						self.eventRegister();
						self.calculateItemAmounts();
        			},
        			error:function(){
    					self.getApp().notify("Get data Eror");
    				},
        		});
    		}else{
    			self.applyBindings();
				self.eventRegister();
				self.calculateItemAmounts();
    		}
    		
    	},
		eventRegister: function(){
			var self = this;
			self.model.on("change:chitiethoadon", function () {
				// check what be changed
				console.log("chitiethoadon change", self.model.get("chitiethoadon"));
				self.calculateItemAmounts();
				
			});
		},
		calculateItemAmounts: function(){
			var self = this;
			var thanhtien = 0;
			for(var i = 0; i < self.model.get("chitiethoadon").length; i++){
				thanhtien = parseInt(self.model.get("chitiethoadon")[i].thanhtien)  + thanhtien;
			}
			self.model.set("thanhtien", thanhtien);
			var vat = self.model.get("vat");
			var tongtien = thanhtien*vat + thanhtien;
			self.model.set("tongtien",tongtien);
		},
    });

});