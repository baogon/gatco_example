define(function (require) {
	"use strict";
	var $ = require('jquery'),
		_ = require('underscore'),
		Gonrin = require('gonrin');
	return [
				
		{
			"collectionName": "role", 
			"route": "role/collection(/:param)",
			"$ref": "app/view/Role/CollectionView",
		},
		{
			"collectionName": "role",
			"route": "role/model(/:param)",
			"$ref": "app/view/Role/ModelView",
		},
		{
			"collectionName": "quocgia", 
			"route": "quocgia/collection(/:param)",
			"$ref": "app/view/QuocGia/CollectionView",
		},
		{
			"collectionName": "quocgia",
			"route": "quocgia/model(/:param)",
			"$ref": "app/view/QuocGia/ModelView",
		},
		{
			"collectionName": "users", 
			"route": "users/collection(/:param)",
			"$ref": "app/view/User/CollectionView",
		},
		{
			"collectionName": "users",
			"route": "users/model(/:param)",
			"$ref": "app/view/User/ModelView",
		},
		{
			"collectionName": "wallet_user", 
			"route": "wallet_user/collection(/:param)",
			"$ref": "app/view/UserWallet/CollectionView",
		},
		{
			"collectionName": "wallet_user",
			"route": "wallet_user/model(/:param)",
			"$ref": "app/view/UserWallet/ModelView",
		},
		{
			"collectionName": "tinhthanh", 
			"route": "tinhthanh/collection(/:param)",
			"$ref": "app/view/TinhThanh/CollectionView",
		},
		{
			"collectionName": "tinhthanh",
			"route": "tinhthanh/model(/:param)",
			"$ref": "app/view/TinhThanh/ModelView",
		},
		{
			"collectionName": "quanhuyen", 
			"route": "quanhuyen/collection(/:param)",
			"$ref": "app/view/QuanHuyen/CollectionView",
		},
		{
			"collectionName": "quanhuyen",
			"route": "quanhuyen/model(/:param)",
			"$ref": "app/view/QuanHuyen/ModelView",
		},
		{
			"collectionName": "xaphuong", 
			"route": "xaphuong/collection(/:param)",
			"$ref": "app/view/XaPhuong/CollectionView",
		},
		{
			"collectionName": "xaphuong",
			"route": "xaphuong/model(/:param)",
			"$ref": "app/view/XaPhuong/ModelView",
		},
		{
			"collectionName": "khachhang", 
			"route": "khachhang/collection(/:param)",
			"$ref": "app/view/KhachHang/CollectionView",
		},
		{
			"collectionName": "khachhang",
			"route": "khachhang/model(/:param)",
			"$ref": "app/view/KhachHang/ModelView",
		},
		{
			"collectionName": "hanghoa", 
			"route": "hanghoa/collection(/:param)",
			"$ref": "app/view/HangHoa/CollectionView",
		},
		{
			"collectionName": "hanghoa",
			"route": "hanghoa/model(/:param)",
			"$ref": "app/view/HangHoa/ModelView",
		},
		{
			"collectionName": "hoadon", 
			"route": "hoadon/collection(/:param)",
			"$ref": "app/view/HoaDon/CollectionView",
		},
		{
			"collectionName": "hoadon",
			"route": "hoadon/model(/:param)",
			"$ref": "app/view/HoaDon/ModelView",
		},
	];

});


